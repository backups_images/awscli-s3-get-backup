FROM alpine:3.5
MAINTAINER Sander Vergeles <sander.vergeles@gmail.com>

ENV DOCKERIZE_VERSION v0.3.0

RUN apk --no-cache update && \
    apk --no-cache add python py-pip py-setuptools ca-certificates wget && \
    pip --no-cache-dir install awscli && \
    \
    # Install DOCKERIZE
	wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm -rf /var/cache/apk/* \
    && mkdir ~/.aws/

COPY ./aws/ /root/.aws/
COPY ./code/ /var/code/

RUN chmod 0755 /var/code/* -R

ENTRYPOINT /bin/sh /var/code/config-template.sh && /bin/sh /var/code/get-s3-backup.sh
