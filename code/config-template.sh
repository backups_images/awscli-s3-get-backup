#!/bin/sh

dockerize -template /root/.aws/config.tmpl:/root/.aws/config -template /root/.aws/credentials.tmpl:/root/.aws/credentials
