#!/bin/sh

# get last backup file
LAST_BACKUP_FILENAME="$(aws s3 ls $AWS_BUCKET --recursive | sort | tail -n 1 | awk '{print $4}')"
TYPE=".tar.gz"

# string length
FILENAME_LENGTH=${#LAST_BACKUP_FILENAME}
TAR_GZ_LENGTH=${#TYPE}

# get type of backup filename 
STRING_TYPE_LENGTH=$((FILENAME_LENGTH - TAR_GZ_LENGTH))
STRING_TYPE_LENGTH=$((STRING_TYPE_LENGTH + 1))
STRING_TYPE=$(echo "$LAST_BACKUP_FILENAME" | awk "{print substr(\$0, $STRING_TYPE_LENGTH)}")

# Validation
if [ $STRING_TYPE == ".tar.gz" ];
then
	if ! aws s3 cp s3://$AWS_BUCKET/$LAST_BACKUP_FILENAME /tmp/$LAST_BACKUP_FILENAME 2>database.err
	then
		echo 'Get s3 backup - download fail'
		echo '---------------'
		cat database.err
		exit 1;
	fi
else
	echo 'Get s3 backup - file format fail'
	exit 1;
fi
