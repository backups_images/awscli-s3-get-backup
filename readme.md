# Backup example:

docker run --rm -i \
  --name awscli_s3_download \
  --net database_network \
  -v /tmp/:/tmp/:rw \
  -e AWS_ACCESS_KEY_ID=???? \
  -e AWS_SECRET_ACCESS_KEY=???? \
  -e AWS_BUCKET=site.ee-database \
  -e AWS_REGION=eu-west-2 \
  registry.gitlab.com/backups_images/awscli-s3-get-backup && \
\
docker run --rm -i \
  --name backup_import_container \
  --net database_network \
  -v /tmp/:/tmp/:rw \
  mariadb:latest \
  bash -c '
    DATABASE_CREATED="NO"
    if [ ! -f /tmp/*.gz ] || [ ! -f /tmp/*.tar.gz ];
    then
      echo "NO backup in /tmp/ folder"
      exit 1;
    fi;

    DATABASE_SHOW=$(echo "show databases;" | mysql --user=homestead --password=secret -h mariadb_container | grep -v "\(Database\|information_schema\|performance_schema\|mysql\|test\)" | grep -o homestead)
    if [ "$DATABASE_SHOW" == "homestead" ];
    then
      if mysql --user=homestead --password=secret -h mariadb_container --execute="DROP DATABASE homestead; CREATE DATABASE homestead;"
      then
        DATABASE_CREATED="YES"
      fi;
    else
      if mysql --user=homestead --password=secret -h mariadb_container -e "CREATE DATABASE homestead"
      then
        DATABASE_CREATED="YES"
      fi;
    fi;
  
    if [ "$DATABASE_CREATED" == "YES" ];
    then
      tar -xzOf /tmp/*.tar.gz | mysql --user=homestead --password=secret -h mariadb_container homestead
      rm -r /tmp/
    fi;
  '
